from flask import Flask, render_template
from flask_socketio import SocketIO, emit
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='gevent') 

# change current directory to package root directory
os.chdir(app.root_path)



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/receive')
def receive():
    return render_template('receive.html')

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)
    emit('message', message, broadcast=True)

@socketio.on('animation')
def handle_animation(message):
    print('received animation: ' + message)
    emit('animation', message, broadcast=True)

def start():
    socketio.run(app, host="0.0.0.0", port=5000)

if __name__ == '__main__':
    start()