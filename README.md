# Stream Flair

Stream Flair is a web-based animation and text overlay tool that adds personality and style to your video streams. It can be used in OBS or other streaming software to create eye-catching effects that engage your viewers and make your streams stand out.

## Features

Some of the features of Stream Flair include:

Confetti and text overlays
Easy integration with OBS

## How to Run

To run Stream Flair, you can use Docker. Here's an example command to start a Docker container:

```bash
docker run -ti -d --restart=always -p5000:5000 aapjeisbaas/stream-flair:latest
```

Once the container is running, you can access Stream Flair by navigating to http://localhost:5000 in your web browser.

## How to add to OBS

[Add a browser source](https://obsproject.com/kb/browser-source) and point it to: `http://127.0.0.1:5000/receive`

## Contributing

If you would like to contribute to Stream Flair, please feel free to submit pull requests or issues on the project's GitLab page. We welcome bug reports, feature requests, and code contributions from the community.

> We hope you enjoy using Stream Flair to enhance your video streams. If you have any questions or feedback, please don't hesitate to contact us.
