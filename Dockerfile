FROM python:bullseye AS builder
USER root
WORKDIR /build
RUN apt update && apt install -y upx-ucl
RUN pip3 install pyinstaller staticx patchelf-wrapper
RUN mkdir requirements
COPY requirements.txt .
RUN pip3 install -r requirements.txt -t requirements
COPY . /build
RUN pyinstaller main.spec


FROM debian:bullseye-slim
WORKDIR /
USER root
COPY --from=builder /build/dist/main /app
CMD ["/app/main"]
EXPOSE 5000/tcp
USER 1001
