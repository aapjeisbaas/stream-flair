#!/bin/bash

cat > static/animations.json << EOF
{
  "animations": [
    {
      "name": "Throw confetti",
      "body": "confetti"
    }
  ]
}
EOF

UA="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"
BASE_COLLECTION_URL="https://lottiefiles.com/collections/cool/1a27a249-38f7-487d-b7b5-a2037451c106"

#ANIMATIONS=$(curl -L -s -H "User-Agent: ${UA}" 'https://lottiefiles.com/collections/cool/1a27a249-38f7-487d-b7b5-a2037451c106?page=1' | grep '<lottie-listings' -A 8 | grep 'src' | sed 's/.*src="//g' | sed 's/"//g')

# while finding next pages
PAGE="?page=1"

while [ -n "$PAGE" ]; do
    echo "Fetching $PAGE"
    ANIMATIONS=$(curl -L -s -H "User-Agent: ${UA}" "${BASE_COLLECTION_URL}${PAGE}" | grep '<lottie-listings' -A 8 -B 4 | grep 'src\|title' | sed 's/.*title="/|/g; s/"\W.*/,/g' | tr -d '\n' | sed 's/[[:space:]]*src="//g; s/"/\n/g; s/|//g')

    while IFS= read -r animation; do
        #echo $animation
        NAME=$(echo $animation | cut -d ',' -f 1)
        URL=$(echo $animation | cut -d ',' -f 2 | sed 's/dotlotties\/dlf10/packages\/lf20/g; s/\.lottie$/\.json/g')
        echo "$NAME - $URL"
        jq --arg name "${NAME}" --arg body "lottie-$URL" '.animations += [{$name,$body}]' static/animations.json > tmp.json && mv tmp.json static/animations.json
    done <<< "$ANIMATIONS"
    
    PAGE=$(curl -L -s -H "User-Agent: ${UA}" "${BASE_COLLECTION_URL}${PAGE}" | grep 'rel="next"' | grep '?page=' |sed 's/.*href="//g; s/" rel=.*//g')

done